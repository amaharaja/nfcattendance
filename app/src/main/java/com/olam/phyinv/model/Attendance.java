package com.olam.phyinv.model;

public class Attendance {

    private String Id;
    private String DeviceId;
    private String TYPE;
    private String Batch;

    private String remark;
    private String Status;

    private String Username;

    private String dateIntiated;
    private String dateClosed;

    private String DEPART;
    private String PAGE;
    private boolean checked = false;

    public String getPAGE() {
        return PAGE;
    }

    public void setPAGE(String PAGE) {
        this.PAGE = PAGE;
    }

    public String getDEPART() {
        return DEPART;
    }

    public void setDEPART(String DEPART) {
        this.DEPART = DEPART;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getBatch() {
        return Batch;
    }

    public void setBatch(String batch) {
        Batch = batch;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getDateIntiated() {
        return dateIntiated;
    }

    public void setDateIntiated(String dateIntiated) {
        this.dateIntiated = dateIntiated;
    }

    public String getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(String dateClosed) {
        this.dateClosed = dateClosed;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}

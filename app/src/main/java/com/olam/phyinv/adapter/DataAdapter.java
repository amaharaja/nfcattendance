package com.olam.phyinv.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olam.phyinv.R;
import com.olam.phyinv.model.Attendance;

import java.util.List;

/**
 * Created by Maharaja on 11/10/2017.
 */
public class DataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = DataAdapter.class.getSimpleName();
    private List<Attendance> commonarraylist;
    private DataListener ackListener;
    // private boolean pageFG = true;

    public DataAdapter(List<Attendance> commonarraylist, DataListener ackListener) {
        this.commonarraylist = commonarraylist;
        this.ackListener = ackListener;
        //pageFG = page;
        Log.d(TAG, "L :" + this.commonarraylist.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder holder = null;
        View view =
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.data_list_item, viewGroup, false);
        holder = new DataViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        try {
            if (viewHolder instanceof DataViewHolder) {
                DataViewHolder holder = (DataViewHolder) viewHolder;
                Attendance data = commonarraylist.get(position);
                holder.item_LL.setTag(position);
                holder.delete_CB.setTag(position);

                holder.txt1.setText(data.getUsername());
                holder.txt2.setText(data.getRemark());
                holder.txt3.setText(data.getDEPART());
                if (data.getDateIntiated() != null && !data.getDateIntiated().trim().equals("")) {
                    holder.txt4.setText(data.getDateIntiated());
                } else holder.txt4.setText(data.getDateClosed());
                holder.delete_CB.setChecked(data.isChecked());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return commonarraylist.size();
    }

    public List<Attendance> getItems() {
        return commonarraylist;
    }

    public void addItem(Attendance data) {
        commonarraylist.add(data);
        notifyItemInserted(commonarraylist.size());
    }

    public void approveItem(int position, int approve) {
        Log.d(TAG, "P : " + position + "D : " + approve);
        Attendance data = commonarraylist.get(position);
        commonarraylist.remove(position);
        commonarraylist.add(position, data);
        notifyItemChanged(position);
    }

    public void updateList(List<Attendance> temp) {
        commonarraylist = temp;
        notifyDataSetChanged();
    }

    public interface DataListener {

        void onItemClick(Attendance episode, int position);

        void onItemLongClick(Attendance episode, int position);
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        TextView txttl1, txttl2, txttl3;
        TextView txt1, txt2, txt3, txt4;
        ImageView done_IV;
        CheckBox delete_CB;
        LinearLayout item_LL;

        public DataViewHolder(View view) {
            super(view);
            item_LL = (LinearLayout) view.findViewById(R.id.item_LL);
            done_IV = (ImageView) view.findViewById(R.id.done_IV);
            txttl1 = (TextView) view.findViewById(R.id.txttl1);
            txttl2 = (TextView) view.findViewById(R.id.txttl2);
            txttl3 = (TextView) view.findViewById(R.id.txttl3);
            txt1 = (TextView) view.findViewById(R.id.txt1);
            txt2 = (TextView) view.findViewById(R.id.txt2);
            txt3 = (TextView) view.findViewById(R.id.txt3);
            txt4 = (TextView) view.findViewById(R.id.txt4);
            delete_CB = (CheckBox) view.findViewById(R.id.delete_CB);

            delete_CB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    commonarraylist.get((int) buttonView.getTag()).setChecked(isChecked);
                }
            });
            item_LL.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ackListener != null) {
                                ackListener.onItemClick(commonarraylist.get((int) v.getTag()), (int) v.getTag());
                            }
                        }
                    });
            item_LL.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (ackListener != null) {
                        ackListener.onItemLongClick(commonarraylist.get((int) v.getTag()), (int) v.getTag());
                    }
                    return false;
                }
            });
        }
    }
}

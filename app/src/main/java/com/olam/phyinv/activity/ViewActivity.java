package com.olam.phyinv.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.olam.phyinv.R;
import com.olam.phyinv.adapter.DataAdapter;
import com.olam.phyinv.db.DBQueries;
import com.olam.phyinv.model.Attendance;
import com.olam.phyinv.util.ExcelToSQLite;
import com.olam.phyinv.util.Utils;

import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.models.sort.SortingTypes;
import droidninja.filepicker.utils.Orientation;

public class ViewActivity extends AppCompatActivity implements DataAdapter.DataListener {
    private static final String TAG = ViewActivity.class.getSimpleName();

    private static final int PERMISSIONS_REQUEST_CODE = 123;
    private DBQueries dbQueries;
    private DataAdapter adapterList;
    private Attendance editData;
    private Dialog dialog, editDialog;
    private ArrayList<String> docPaths = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProgressDialog mProgressDialog;
    private RelativeLayout rl_main;
    private List<Attendance> commonarraylist = new ArrayList<>();
    private List<Attendance> temp = new ArrayList<>();
    private TextView view1, view2, view3, view4, view5;
    private EditText eview1, eview2, eview3, eview4, eview5;
    private EditText textLANE;
    private TextInputLayout til3;
    private ImageView viewSerach;
    private boolean pageFG = true;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        dbQueries = new DBQueries(getApplicationContext());
        pref = getSharedPreferences(Utils.APPPREF, MODE_PRIVATE);
        pageFG = pref.getBoolean(Utils.PREF_FG, true);

        initViews();
        getData();
    }

    public void onReadClick(View view) {
        checkPermissionsAndOpenFilePicker();
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            onPickDoc();
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPickDoc();
                } else {
                    showError();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    for (String s : docPaths) {
                        //onReadData(s);
                        readAndImDB(s);
                    }
                }
                break;
        }
    }

    private void readAndImDB(String directory_path) {
        try {
            // if you want to add column in excel and import into DB, you must drop the table
            ExcelToSQLite excelToSQLite = new ExcelToSQLite(getApplicationContext(), false);
            // Import EXCEL FILE to SQLite
            excelToSQLite.importFromFile(directory_path, new ExcelToSQLite.ImportListener() {
                @Override
                public void onStart() {
                    Utils.showSnackBar(rl_main, "Excel imported start ");

                }

                @Override
                public void onCompleted(String dbName) {
                    Utils.showSnackBar(rl_main, "Excel imported into " + dbName);
                    getData();
                }

                @Override
                public void onProgress(String per) {
                    Utils.showSnackBar(rl_main, "Excel importing " + per);

                }

                @Override
                public void onError(Exception e) {
                    Utils.showSnackBar(rl_main, "Error : " + e.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onPickDoc() {
        String[] xlsx = {".xlsx"};
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(docPaths)
                // .setActivityTheme(R.style.FilePickerTheme)
                .addFileSupport("XLSX", xlsx)
                .enableDocSupport(false)
                .sortDocumentsBy(SortingTypes.name)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickFile(this);
    }


    private void initViews() {

        til3 = (TextInputLayout) findViewById(R.id.til3);
        textLANE = (EditText) findViewById(R.id.txtlane);
        viewSerach = (ImageView) findViewById(R.id.iv_search);
        viewSerach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(textLANE.getText().toString())) {
                    textLANE.setError("Please enter the name");
                    textLANE.requestFocus();
                    return;
                } else filter(textLANE.getText().toString());
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

    }

    private void getData() {
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Fetching data..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            new getData_Db().execute(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(Attendance data, int p) {
        Log.d(TAG, "onItemClick");
        editData = null;
        if (dialog == null) {
            dialog = new Dialog(this,
                    android.R.style.DeviceDefault_Light_ButtonBar); // making dialog full screen
            //inflating dialog layout
            dialog.setContentView(R.layout.dialog_item);
            view1 = (TextView) dialog.findViewById(R.id.txt1);
            view2 = (TextView) dialog.findViewById(R.id.txt2);
            view3 = (TextView) dialog.findViewById(R.id.txt3);
            view4 = (TextView) dialog.findViewById(R.id.txt4);
            view5 = (TextView) dialog.findViewById(R.id.txt5);
        }
        if (dialog != null && !dialog.isShowing()) {
            view1.setText(data.getTYPE());
            view2.setText(data.getBatch());
            view3.setText(data.getRemark());
            view5.setText(data.getStatus());
            dialog.show();
        }
    }

    @Override
    public void onItemLongClick(Attendance data, int p) {
        Log.d(TAG, "onItemLongClick");
        editData = data;
        if (editDialog == null) {
            editDialog = new Dialog(this,
                    android.R.style.DeviceDefault_Light_ButtonBar); // making dialog full screen
            //inflating dialog layout
            editDialog.setContentView(R.layout.edit_item);
            eview1 = (EditText) editDialog.findViewById(R.id.txt1);
            eview2 = (EditText) editDialog.findViewById(R.id.txt2);
            eview3 = (EditText) editDialog.findViewById(R.id.txt3);
            eview4 = (EditText) editDialog.findViewById(R.id.txt4);
            eview5 = (EditText) editDialog.findViewById(R.id.txt5);
        }
        if (editDialog != null && !editDialog.isShowing()) {
            eview1.setText(data.getTYPE());
            eview2.setText(data.getBatch());
            eview3.setText(data.getRemark());
            eview5.setText(data.getStatus());
            editDialog.show();
        }
    }

    public void onUpdateClick(View view) {
        try {
            if (editData != null) {
                editData.setTYPE(eview1.getText().toString());
                editData.setBatch(eview2.getText().toString());
                editData.setRemark(eview3.getText().toString());
                editData.setStatus(eview4.getText().toString());

                dbQueries.open();
                if (dbQueries.updateRemark(editData)) {
                    editDialog.dismiss();
                    editData = null;
                    getData();
                }
                dbQueries.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDeleteClick(View view) {
        Log.d(TAG, "onDeleteClick");
        try {
            if (editData != null) {
                Log.d(TAG, "onDeleteClick" + editData.getId());
                editData.setTYPE(eview1.getText().toString());
                editData.setBatch(eview2.getText().toString());
                editData.setRemark(eview3.getText().toString());
                editData.setStatus(eview4.getText().toString());

                dbQueries.open();
                if (dbQueries.deleteData(editData)) {
                    Log.d(TAG, "onDeleteClick success" + editData.getId());
                    editDialog.dismiss();
                    editData = null;
                    getData();
                }
                dbQueries.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void filter(String text) {
        Log.d(TAG, "filter");
        try {
            if (commonarraylist != null && !commonarraylist.isEmpty()) {
                if (temp != null) {
                    temp = new ArrayList<>();
                } else {
                    temp.clear();
                }
                for (Attendance d : commonarraylist) {
                    // or use .equal(text) with you want equal match
                    // use .toLowerCase() for better matches
                    if (d.getDEPART().toLowerCase().contains(text.toLowerCase())) {
                        temp.add(d);
                    }
                }
                // update recyclerview
                adapterList.updateList(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class getData_Db extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected Integer doInBackground(final Integer... params) {
            try {
                dbQueries.open();
                commonarraylist = dbQueries.readData(pref.getString(Utils.PREF_USER, "Raja"),
                        pref.getString(Utils.PREF_USER_TYPE, "Students"));
                dbQueries.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 1;
        }

        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                adapterList = new DataAdapter(commonarraylist, ViewActivity.this);
                recyclerView.setAdapter(adapterList);
                Log.d(TAG, "L : " + commonarraylist.size());
                adapterList.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
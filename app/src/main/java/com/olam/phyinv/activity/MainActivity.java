package com.olam.phyinv.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.olam.phyinv.BuildConfig;
import com.olam.phyinv.R;
import com.olam.phyinv.db.DBQueries;
import com.olam.phyinv.model.Attendance;
import com.olam.phyinv.util.Mail;
import com.olam.phyinv.util.Utils;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_CODE = 123;

    public static String GMAIL_USER_NAME = "work.maharaja@gmail.com";
    public static String GMAIL_RAJA_NAME = "work.maharaja@gmail.com";
    public static String GMAIL_PASSWORD = "pwd@123";

    private File file = null;
    private DBQueries dbQueries;
    private SharedPreferences pref;
    private List<Attendance> inventories;
    private ProgressDialog mProgressDialog;
    private RelativeLayout rl_main;
    private boolean pageFG = true;
    private boolean viewClicked = true;

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            rl_main = (RelativeLayout) findViewById(R.id.rl_main);
            dbQueries = new DBQueries(getApplicationContext());
            pref = getSharedPreferences(Utils.APPPREF, MODE_PRIVATE);
            pageFG = pref.getBoolean(Utils.PREF_FG, true);

            initViews();
        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
        }
    }

    private void initViews() {

        RadioGroup rg = (RadioGroup) findViewById(R.id.rdogrp);
        RadioButton rb1 = (RadioButton) findViewById(R.id.btn1);
        RadioButton rb2 = (RadioButton) findViewById(R.id.btn2);
        rb1.setText(pref.getString(Utils.PREF_USER_TYPE, "User"));
        rb2.setText((pref.getString(Utils.PREF_USER, "Name")));

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.btn1:
                        pref.edit().putBoolean(Utils.PREF_FG, true).commit();
                        break;
                    case R.id.btn2:
                        pref.edit().putBoolean(Utils.PREF_FG, false).commit();
                        break;
                    default:
                        // do operations specific to this selection
                        break;
                }
                pageFG = pref.getBoolean(Utils.PREF_FG, true);
            }
        });

        Button button = (Button) findViewById(R.id.import_B);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.view_B);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.logout_B);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_B:
                startActivity(new Intent(MainActivity.this, ViewActivity.class));
                break;
            case R.id.import_B:
                viewClicked = true;
                checkPermissionsAndOpenGenerateExcel();
                break;
            case R.id.logout_B:
                pref.edit().clear().commit();
                startActivity(new Intent(this, LogonActivity.class));
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsAndOpenGenerateExcel();
                } else {
                    showError();
                }
            }
        }
    }

    private boolean saveExcelFile(Context context, String fileName) {

        // check if available and not read only
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            Log.e(TAG, "Storage not available or read only");
            return false;
        }

        boolean success = false;

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell c = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        dbQueries.open();
        List<String> listLANE = dbQueries.readLANEData(pref.getString(Utils.PREF_USER, "Raja"),
                pref.getString(Utils.PREF_USER_TYPE, "Students"));

        Log.d("LANE", " = " + listLANE.size());
        for (int i = 0; i < listLANE.size(); i++) {
            String lane = listLANE.get(i);
            Log.d("LANE", " = " + lane);

            //New Sheet
            Sheet sheet = wb.createSheet(lane);

            // Generate column headings
            Row rowheader = sheet.createRow(0);

            c = rowheader.createCell(0);
            c.setCellValue("ID");
            c.setCellStyle(cs);

            c = rowheader.createCell(1);
            c.setCellValue("Batch");
            c.setCellStyle(cs);

            c = rowheader.createCell(2);
            c.setCellValue("Department");
            c.setCellStyle(cs);

            c = rowheader.createCell(3);
            c.setCellValue("Name");
            c.setCellStyle(cs);

            c = rowheader.createCell(4);
            c.setCellValue("Phone");
            c.setCellStyle(cs);

            c = rowheader.createCell(5);
            c.setCellValue("Date initiated");
            c.setCellStyle(cs);

            c = rowheader.createCell(6);
            c.setCellValue("Date closed");
            c.setCellStyle(cs);

            c = rowheader.createCell(7);
            c.setCellValue("Remarks");
            c.setCellStyle(cs);

            sheet.setColumnWidth(0, (15 * 500));
            sheet.setColumnWidth(1, (15 * 500));
            sheet.setColumnWidth(2, (15 * 500));
            sheet.setColumnWidth(3, (15 * 500));
            sheet.setColumnWidth(4, (15 * 500));
            sheet.setColumnWidth(5, (15 * 500));
            sheet.setColumnWidth(6, (15 * 500));
            sheet.setColumnWidth(7, (15 * 500));

            inventories = dbQueries.readROWLANEData(pref.getString(Utils.PREF_USER, "Raja"), lane);
            Log.d("Attendance", " = " + inventories.size());
            for (int j = 0; j < inventories.size(); j++) {
                Attendance attendance = inventories.get(j);
                Log.d("attendance", attendance.getBatch() + " = " + attendance.getId());
                // Generate column
                Row row = sheet.createRow(j + 1);

                c = row.createCell(0);
                c.setCellValue(attendance.getDeviceId() + "");

                c = row.createCell(1);
                c.setCellValue(attendance.getBatch());


                c = row.createCell(2);
                c.setCellValue(attendance.getDEPART());

                c = row.createCell(3);
                c.setCellValue(attendance.getUsername());

                c = row.createCell(4);
                c.setCellValue(attendance.getTYPE());

                c = row.createCell(5);
                c.setCellValue(attendance.getDateIntiated());

                c = row.createCell(6);
                c.setCellValue(attendance.getDateClosed());

                c = row.createCell(7);
                c.setCellValue(attendance.getRemark());

            }

        }
        dbQueries.close();

        // Create a path where we will place our List of objects on external storage
        file = new File(context.getExternalFilesDir(null), fileName);
        /*File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/reports");
        dir.mkdirs();
        file = new File(dir, fileName);
*/

        FileOutputStream os = null;

        try {
            os = new FileOutputStream(file);
            wb.write(os);
            Log.w("FileUtils", "Writing file" + file);
            success = true;
        } catch (IOException e) {
            Log.w("FileUtils", "Error writing " + file, e);
        } catch (Exception e) {
            Log.w("FileUtils", "Failed to save file", e);
        } finally {
            try {
                if (null != os)
                    os.close();
            } catch (Exception ex) {
            }
        }
        pref.edit().putString(Utils.PREF_FILE, fileName).commit();
        return success;
    }


    private void getGenerateExcel() {
        try {
            dbQueries.open();
            List<String> listLANE = dbQueries.readLANEData(pref.getString(Utils.PREF_USER, "Raja"),
                    pref.getString(Utils.PREF_USER_TYPE, "Students"));
            if (listLANE.isEmpty()) {
                Toast.makeText(this, "No data Added.", Toast.LENGTH_SHORT).show();
                dbQueries.close();
            } else {
                dbQueries.close();

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Generate excel report");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                new GenerateExcel().execute(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String sendMail(File attachment, String v_userName, String to) {
        try {
            Mail m = new Mail(GMAIL_USER_NAME, GMAIL_PASSWORD);
            String[] toArr = {to};
            String[] ccArr = {GMAIL_USER_NAME, GMAIL_RAJA_NAME};
            m.setTo(toArr);
            m.setCC(ccArr);
            m.setFrom(GMAIL_USER_NAME);
            m.setSubject("History Report");
            m.setBody("Dear Team, \n \nPlease find the attached History Report. \n \nRegards,\n"
                    + v_userName);
            try {
                m.addAttachment(attachment.getAbsolutePath(), pref.getString(Utils.PREF_FILE, "report"));

                if (m.send()) {
                    // Toast.makeText(SendFileInMail.this,
                    // "Email was sent successfully.",
                    // Toast.LENGTH_LONG).show();
                    dbQueries.open();
                    dbQueries.updateStatus(inventories);
                    dbQueries.close();
                    return "Email was sent successfully";
                } else {
                    // Toast.makeText(SendFileInMail.this,
                    // "Email was not sent.", Toast.LENGTH_LONG).show();
                    return "Email not sent";
                }
            } catch (Exception e) {
                // Toast.makeText(MailApp.this,
                // "There was a problem sending the email.",
                // Toast.LENGTH_LONG).show();
                Log.e("MailApp", "Could not send email", e);
                return "There was a problem sending the email" + e;
            }
        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
            // Toast.makeText(SendFileInMail.this, "Email was not sent."+e, Toast.LENGTH_SHORT).show();
            return "Email was not sent" + e;
        }
    }

    private void checkPermissionsAndOpenGenerateExcel() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            getGenerateExcel();
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    private void openFile(Context context, String url) {

        try {
            // Create URI
            File file = new File(context.getExternalFilesDir(null), url);
            //File root = Environment.getExternalStorageDirectory();
            //File file = new File(root.getAbsolutePath() + "/reports/" + url);

            // Uri uri = Uri.fromFile(fileout);

            Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "No Application found to open file", Toast.LENGTH_SHORT).show();
        }
    }

    private void showToAddressDialog() {
        final String re = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Enter To Email Address : ");
        final EditText userid = new EditText(this);
        userid.setText(pref.getString(Utils.PREF_TOMAIL, ""));
        userid.setHint("Enter Mail address");
        userid.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        alert.setView(userid);
        alert.setCancelable(false);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String em = userid.getText().toString();
                if (em.length() > 0 && Pattern.compile(re).matcher(em).matches()) {
                    pref.edit().putString(Utils.PREF_TOMAIL, em).commit();
                    String msg = sendMail(file, pref.getString(Utils.PREF_USER, ""), em);
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Enter valid to email address.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alert.show();
    }

    private void openExcel() {

        if (TextUtils.isEmpty(pref.getString(Utils.PREF_FILE, ""))) {
            Toast.makeText(MainActivity.this, "No file available", Toast.LENGTH_SHORT).show();
        } else
            openFile(MainActivity.this, pref.getString(Utils.PREF_FILE, ""));

    }

    private class GenerateExcel extends AsyncTask<Integer, Void, Integer> {
        boolean success = false;

        @Override
        protected Integer doInBackground(final Integer... params) {
            Long tsLong = System.currentTimeMillis() / 1000;
            success = saveExcelFile(MainActivity.this, tsLong.toString() + "Report.xls");
            return 1;
        }

        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
            try {

                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                if (viewClicked) {
                    openExcel();
                } else {
                    if (file != null && success) {
                        //showToAddressDialog();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
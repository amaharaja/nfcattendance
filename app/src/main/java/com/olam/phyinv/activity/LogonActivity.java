package com.olam.phyinv.activity;

import android.Manifest;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.SQLException;
import android.nfc.NfcAdapter;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.olam.phyinv.R;
import com.olam.phyinv.db.DBQueries;
import com.olam.phyinv.model.Attendance;
import com.olam.phyinv.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import be.appfoundry.nfclibrary.utilities.sync.NfcReadUtilityImpl;

public class LogonActivity extends AppCompatActivity {
    private static final String TAG = LogonActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    private PendingIntent pendingIntent;
    private IntentFilter[] mIntentFilters;
    private String[][] mTechLists;
    private NfcAdapter mNfcAdapter;

    private String Remark = "In";
    private RelativeLayout rl_main;
    private SharedPreferences pref;
    private EditText txt1, txt2;
    private TextView timer;
    private DBQueries dbQueries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dbQueries = new DBQueries(getApplicationContext());

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        mIntentFilters = new IntentFilter[]{new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)};
        mTechLists = new String[][]{new String[]{Ndef.class.getName()},
                new String[]{NdefFormatable.class.getName()}};


        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        txt1 = (EditText) findViewById(R.id.txt1);
        txt2 = (EditText) findViewById(R.id.txt2);
        timer = (TextView) findViewById(R.id.timecount);

        pref = getSharedPreferences(Utils.APPPREF, MODE_PRIVATE);

        if (!TextUtils.isEmpty(pref.getString(Utils.PREF_USER, "")) &&
                !TextUtils.isEmpty(pref.getString(Utils.PREF_USER_TYPE, ""))) {
            startActivity(new Intent(LogonActivity.this, MainActivity.class));
            finish();
            return;
        }

        Button btn = (Button) findViewById(R.id.btn_login);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = txt1.getText().toString().trim();
                String pwd = txt2.getText().toString().trim();
                if (TextUtils.isEmpty(user)) {
                    txt1.setError("Can't empty name");
                    txt1.requestFocus();
                } else if (TextUtils.isEmpty(pwd)) {
                    txt2.setError("Can't empty password");
                    txt2.requestFocus();
                } else {

                    if (user.toLowerCase().equalsIgnoreCase("admin")
                            && pwd.toLowerCase().equalsIgnoreCase("adminpwd")) {
                        pref.edit().putString(Utils.PREF_USER_TYPE, "Admin").commit();

                    } else if (user.toLowerCase().equalsIgnoreCase("cse")
                            && pwd.toLowerCase().equalsIgnoreCase("csepwd")) {
                        pref.edit().putString(Utils.PREF_USER_TYPE, "CSE").commit();

                    } else if (user.toLowerCase().equalsIgnoreCase("eee")
                            && pwd.toLowerCase().equalsIgnoreCase("eeepwd")) {
                        pref.edit().putString(Utils.PREF_USER_TYPE, "EEE").commit();

                    } else if (user.toLowerCase().equalsIgnoreCase("ece")
                            && pwd.toLowerCase().equalsIgnoreCase("ecepwd")) {
                        pref.edit().putString(Utils.PREF_USER_TYPE, "ECE").commit();

                    } else {
                        dbQueries.open();
                        if (dbQueries.validateUser(user, pwd)) {
                            pref.edit().putString(Utils.PREF_USER_TYPE, "Student").commit();
                            dbQueries.close();
                        } else {
                            dbQueries.close();
                            Toast.makeText(LogonActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    pref.edit().putString(Utils.PREF_USER, txt1.getText().toString()).commit();

                    if (!TextUtils.isEmpty(pref.getString(Utils.PREF_USER, "")) &&
                            !TextUtils.isEmpty(pref.getString(Utils.PREF_USER_TYPE, ""))) {
                        startActivity(new Intent(LogonActivity.this, MainActivity.class));
                        finish();
                    }

                }
            }
        });

        dialogTime();
        if (!hasReadSmsPermission()) {
            requestReadAndSendSmsPermission();
        }
    }

    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            Log.d(TAG, "shouldShowRequestPermissionRationale(), no permission requested");
            return;
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS},
                MY_PERMISSIONS_REQUEST_SEND_SMS);
    }

    /**
     * Runtime permission shenanigans
     */
    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   /* Toast.makeText(getApplicationContext(), "SMS sent permission granted.",
                            Toast.LENGTH_LONG).show();*/
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    protected void sendSMSMessage(String message, String phoneNo) {
        if (phoneNo.equals("")) {
            Toast.makeText(this, "Please add number", Toast.LENGTH_SHORT).show();
            return;
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }

        } else {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "Data sent.",
                    Toast.LENGTH_LONG).show();
        }
    }

    private void dialogTime() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Alert.!");
        alert.setCancelable(false);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);

        final Switch sw = new Switch(this);
        sw.setTextOn("In");
        sw.setTextOff("Out");

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView view = new TextView(this);
        view.setText("Is morning.!");
        linearLayout.addView(view);
        linearLayout.addView(sw);
        TextView view1 = new TextView(this);
        view1.setText("set countdown time");
        linearLayout.addView(view1);
        linearLayout.addView(input);

        alert.setView(linearLayout);

        alert.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Remark = sw.isChecked() ? "In" : "Out";
                if (TextUtils.isEmpty(input.getText().toString().trim())) {
                    input.setError("Conn't empty time");
                    input.requestFocus();
                    setTimeShift(20);
                } else {
                    int time = Integer.valueOf(input.getText().toString().trim());
                    setTimeShift(time);
                }
            }
        });
        alert.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Remark = sw.isChecked() ? "In" : "Out";
                if (TextUtils.isEmpty(input.getText().toString().trim())) {
                    input.setError("Conn't empty time");
                    input.requestFocus();
                    setTimeShift(20);
                } else {
                    int time = Integer.valueOf(input.getText().toString().trim());
                    setTimeShift(time);
                }
            }
        });
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Remark = sw.isChecked() ? "In" : "Out";
                if (TextUtils.isEmpty(input.getText().toString().trim())) {
                    input.setError("Conn't empty time");
                    input.requestFocus();
                    setTimeShift(20);
                } else {
                    int time = Integer.valueOf(input.getText().toString().trim());
                    setTimeShift(time);
                }
            }
        });
        alert.show();
    }

    private void setTimeShift(int time) {
        new CountDownTimer(TimeUnit.MINUTES.toMillis(time), 1000) {
            public void onTick(long millis) {
                //here you can have your logic to set text to edittext
                try {
                    String millisUntilFinished = String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis) -
                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                            TimeUnit.MILLISECONDS.toSeconds(millis) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                    timer.setText(millisUntilFinished);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFinish() {
                try {
                    timer.setText("done! \n Remaining people's send sms alert to parents mobile");
                    getAbsentStudent();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.start();
    }

    private void getAbsentStudent() {
        dbQueries.open();
        for (Attendance attendance : dbQueries.readUser()) {
            String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            if (!dbQueries.isPersent(attendance.getId(), today)) {
                String msg = "Today' " + attendance.getUsername() + " Absent class";
                sendSMSMessage(msg, attendance.getTYPE());
            }
        }
        dbQueries.close();
    }

    public void onResume() {
        super.onResume();
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(this, pendingIntent, mIntentFilters, mTechLists);
        }
    }

    public void onPause() {
        super.onPause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            SparseArray<String> res = new NfcReadUtilityImpl().readFromTagWithSparseArray(intent);
            for (int i = 0; i < res.size(); i++) {
                String val = res.valueAt(i);
                //Toast.makeText(this, val, Toast.LENGTH_SHORT).show();
                String[] data = val.split("'");
                //id , batch , department , name,mobile nu
                if (data.length == 5) {
                    insertData(data[0], data[1], data[2], data[3], data[4]);
                } else {
                    Toast.makeText(this, "Invalid data", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertData(String id, String batch, String depart, String name, String num) {
        try {
            dbQueries.open();

            if (!dbQueries.checkuser(id, batch, depart, name)) {
                if (dbQueries.userAdd(id, batch, depart, name, num))
                    Toast.makeText(this, "Student added", Toast.LENGTH_SHORT).show();
            }

            Attendance attendance = new Attendance();
            attendance.setDeviceId(id);
            attendance.setBatch(batch);
            attendance.setDEPART(depart);
            attendance.setUsername(name);
            attendance.setTYPE(num);
            attendance.setRemark(Remark);

            // Convert input Date into a String
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            String date = inputFormat.format(new Date());

            if (Remark.equalsIgnoreCase("In")) {
                attendance.setDateIntiated(date);
            } else {
                attendance.setDateClosed(date);
            }
            attendance.setStatus("P");
            attendance.setPAGE(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

            if (dbQueries.insertData(attendance) > -1)
                Toast.makeText(this, "Student check " + Remark + " Added", Toast.LENGTH_SHORT).show();

            dbQueries.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
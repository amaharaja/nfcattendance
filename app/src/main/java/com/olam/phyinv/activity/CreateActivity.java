package com.olam.phyinv.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.olam.phyinv.R;
import com.olam.phyinv.adapter.DataAdapter;
import com.olam.phyinv.db.DBQueries;
import com.olam.phyinv.model.Attendance;
import com.olam.phyinv.util.ExcelToSQLite;
import com.olam.phyinv.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import droidninja.filepicker.FilePickerConst;

public class CreateActivity extends AppCompatActivity implements DataAdapter.DataListener {
    private static final String TAG = CreateActivity.class.getSimpleName();

    private static final int PERMISSIONS_REQUEST_CODE = 123;
    private DBQueries dbQueries;
    private DataAdapter adapterList;
    private Attendance editData;
    private Dialog dialog, editDialog;
    private ArrayList<String> docPaths = new ArrayList<>();
    private RecyclerView recyclerView;
    private RelativeLayout rl_main;
    private List<Attendance> commonarraylist = new ArrayList<>();
    private TextView view1, view2, view3, view4, view5;
    private EditText eview1, eview2, eview3, eview4, eview5;
    private EditText textLANE, textBatch, textQty;
    private CheckBox cb_edit;
    private ImageView iv_Done;
    private TextInputLayout til1, til2, til3;
    private SharedPreferences pref;
    private int position;
    private boolean pageFG = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        pref = getSharedPreferences(Utils.APPPREF, MODE_PRIVATE);
        pageFG = pref.getBoolean(Utils.PREF_FG, true);

        dbQueries = new DBQueries(getApplicationContext());
        initViews();
        checkPermissionsAndOpenFilePicker();
    }

    public void onReadClick(View view) {
        checkPermissionsAndOpenFilePicker();
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_PHONE_STATE;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            // onPickDoc();
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //onPickDoc();
                } else {
                    showError();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    for (String s : docPaths) {
                        //onReadData(s);
                        readAndImDB(s);
                    }
                }
                break;
        }
    }

    private void readAndImDB(String directory_path) {
        try {
            // if you want to add column in excel and import into DB, you must drop the table
            ExcelToSQLite excelToSQLite = new ExcelToSQLite(getApplicationContext(), false);
            // Import EXCEL FILE to SQLite
            excelToSQLite.importFromFile(directory_path, new ExcelToSQLite.ImportListener() {
                @Override
                public void onStart() {
                    Utils.showSnackBar(rl_main, "Excel imported start ");

                }

                @Override
                public void onCompleted(String dbName) {
                    Utils.showSnackBar(rl_main, "Excel imported into " + dbName);
                    //getData();
                }

                @Override
                public void onProgress(String per) {
                    Utils.showSnackBar(rl_main, "Excel importing " + per);

                }

                @Override
                public void onError(Exception e) {
                    Utils.showSnackBar(rl_main, "Error : " + e.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initViews() {
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            textLANE = (EditText) findViewById(R.id.txtlane);
            textBatch = (EditText) findViewById(R.id.txtbatch);
            til1 = (TextInputLayout) findViewById(R.id.til1);
            til2 = (TextInputLayout) findViewById(R.id.til2);
            til3 = (TextInputLayout) findViewById(R.id.til3);

            textQty = (EditText) findViewById(R.id.txtqty);
            cb_edit = (CheckBox) findViewById(R.id.edit_CB);
            iv_Done = (ImageView) findViewById(R.id.iv_done);

            if (pageFG) {
                textQty.setVisibility(View.GONE);
                //cb_edit.setVisibility(View.VISIBLE);
                til1.setVisibility(View.GONE);
                til2.setHint("Batch");
                til3.setHint("LANE");
                cb_edit.setChecked(false);

            } else {
                textQty.setVisibility(View.VISIBLE);
                //cb_edit.setVisibility(View.GONE);
                til1.setVisibility(View.VISIBLE);
                til2.setHint("Material");
                til3.setHint("Sloc");
                cb_edit.setChecked(true);
            }

            InputMethodManager imm = (InputMethodManager) rl_main.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (cb_edit.isChecked()) {
                iv_Done.setVisibility(View.VISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= 11) {
                    textBatch.setRawInputType(InputType.TYPE_CLASS_TEXT);
                    textBatch.setTextIsSelectable(true);
                } else {
                    textBatch.setRawInputType(InputType.TYPE_NULL);
                    textBatch.setFocusable(true);
                }
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                imm.showSoftInput(textBatch, InputMethodManager.SHOW_FORCED);
            } else {
                imm.hideSoftInputFromWindow(rl_main.getWindowToken(), 0);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                iv_Done.setVisibility(pageFG ? View.GONE : View.VISIBLE);
            }

            iv_Done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(textLANE.getText().toString())) {
                        textLANE.setError(pageFG ? "Please enter the Lane number" : "Please enter the Sloc number");
                        textLANE.requestFocus();
                        return;
                    }
                    if (!pageFG) {
                        if (TextUtils.isEmpty(textQty.getText().toString())) {
                            textQty.setError("Please enter the Qty number");
                            textQty.requestFocus();
                            return;
                        }
                    }

                    if (!TextUtils.isEmpty(textBatch.getText().toString())) {
                        Attendance attendance = new Attendance();
                        attendance.setPAGE(pageFG ? "FG" : "NonFG");
                        attendance.setStatus("V");

                        attendance.setBatch(textBatch.getText().toString());
                        attendance.setDEPART(textBatch.getText().toString());

                        attendance.setTYPE(textLANE.getText().toString());

                        // Convert input Date into a String
                        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        String date = inputFormat.format(new Date());
                        attendance.setRemark("");
                        attendance.setDateIntiated(date);
                        attendance.setDateClosed(date);
                        attendance.setUsername(pref.getString(Utils.PREF_USER, ""));
                        attendance.setDeviceId("not have permission");

                        TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                        if (ContextCompat.checkSelfPermission(CreateActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(CreateActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                                showError();
                            } else {
                                ActivityCompat.requestPermissions(CreateActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_CODE);
                            }
                        } else {
                            attendance.setDeviceId(mTelephonyMgr.getDeviceId());
                        }
                        dbQueries.open();

                        int i = (int) dbQueries.insertData(attendance);
                        if (i > -1) {
                            attendance.setId("" + i);
                            getData(attendance);
                        }
                        dbQueries.close();
                        textBatch.setText("");
                        textQty.setText("");

                        textBatch.clearFocus();
                        textBatch.requestFocus();
                    }
                }
            });

            cb_edit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    InputMethodManager imm = (InputMethodManager) rl_main.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (cb_edit.isChecked()) {
                        iv_Done.setVisibility(View.VISIBLE);
                        if (android.os.Build.VERSION.SDK_INT >= 11) {
                            textBatch.setRawInputType(InputType.TYPE_CLASS_TEXT);
                            textBatch.setTextIsSelectable(true);
                        } else {
                            textBatch.setRawInputType(InputType.TYPE_NULL);
                            textBatch.setFocusable(true);
                        }
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        imm.showSoftInput(textBatch, InputMethodManager.SHOW_FORCED);
                    } else {
                        imm.hideSoftInputFromWindow(rl_main.getWindowToken(), 0);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        iv_Done.setVisibility(pageFG ? View.GONE : View.VISIBLE);
                    }
                }
            });

            textBatch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    InputMethodManager imm = (InputMethodManager) rl_main.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (cb_edit.isChecked()) {
                        iv_Done.setVisibility(View.VISIBLE);
                        imm.showSoftInput(textBatch, InputMethodManager.SHOW_FORCED);
                    } else {
                        imm.hideSoftInputFromWindow(rl_main.getWindowToken(), 0);
                        if (android.os.Build.VERSION.SDK_INT >= 11) {
                            textBatch.setRawInputType(InputType.TYPE_CLASS_TEXT);
                            textBatch.setTextIsSelectable(true);
                        } else {
                            textBatch.setRawInputType(InputType.TYPE_NULL);
                            textBatch.setFocusable(true);
                        }
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        iv_Done.setVisibility(pageFG ? View.GONE : View.VISIBLE);
                    }
                }
            });

            textBatch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    Log.d(TAG, "beforeTextChanged");
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Log.d(TAG, "onTextChanged");
                    try {
                        if (TextUtils.isEmpty(textLANE.getText().toString())) {
                            textLANE.setError(pageFG ? "Please enter the Lane number" : "Please enter the Sloc number");
                            textLANE.requestFocus();
                        } else if (TextUtils.isEmpty(textBatch.getText().toString())) {
                            textBatch.setError(pageFG ? "Please enter or scan the Batch number" : "Please enter or scan the Material number");
                            textBatch.requestFocus();
                        } else if (TextUtils.isEmpty(textBatch.getText().toString().trim())) {
                            textBatch.setError(pageFG ? "Please enter or scan the Batch number" : "Please enter or scan the Material number");
                            textBatch.requestFocus();
                        } else if (pageFG && !TextUtils.isEmpty(textBatch.getText().toString().trim()) && !cb_edit.isChecked()) {
                            Attendance attendance = new Attendance();

                            attendance.setPAGE(pageFG ? "FG" : "NonFG");
                            attendance.setStatus("V");

                            attendance.setBatch(textBatch.getText().toString().trim());
                            attendance.setTYPE(textLANE.getText().toString().trim());
                            // Convert input Date into a String
                            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                            String date = inputFormat.format(new Date());
                            attendance.setRemark("");
                            attendance.setDateIntiated(date);
                            attendance.setDateClosed(date);
                            attendance.setUsername(pref.getString(Utils.PREF_USER, ""));

                            attendance.setDeviceId("not have permission");

                            TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                            if (ContextCompat.checkSelfPermission(CreateActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(CreateActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                                    showError();
                                } else {
                                    ActivityCompat.requestPermissions(CreateActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_CODE);
                                }
                            } else {
                                attendance.setDeviceId(mTelephonyMgr.getDeviceId());
                            }

                            dbQueries.open();
                            int i = (int) dbQueries.insertData(attendance);
                            if (i > -1) {
                                attendance.setId("" + i);
                                getData(attendance);
                            }
                            dbQueries.close();
                            textBatch.setText("");
                        } else {
                            textQty.setText("");
                            textQty.requestFocus();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Log.d(TAG, "afterTextChanged");

                }
            });

            recyclerView = (RecyclerView) findViewById(R.id.list);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getData(Attendance attendance) {
        try {
            textLANE.setEnabled(false);
            // Collections.reverse(commonarraylist);
            commonarraylist.add(0, attendance);
            adapterList = new DataAdapter(commonarraylist, CreateActivity.this);
            recyclerView.setAdapter(adapterList);
            Log.d(TAG, "L : " + commonarraylist.size());
            adapterList.notifyDataSetChanged();
/*
            textBatch.setText("");
            textBatch.clearFocus();
            textBatch.requestFocus();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(Attendance data, int position) {
        Log.d(TAG, "onItemClick " + position);
        editData = null;
        this.position = position;
        if (dialog == null) {
            dialog = new Dialog(this,
                    android.R.style.DeviceDefault_Light_ButtonBar); // making dialog full screen
            //inflating dialog layout
            dialog.setContentView(R.layout.dialog_item);
            view1 = (TextView) dialog.findViewById(R.id.txt1);
            view2 = (TextView) dialog.findViewById(R.id.txt2);
            view3 = (TextView) dialog.findViewById(R.id.txt3);
            view4 = (TextView) dialog.findViewById(R.id.txt4);
            view5 = (TextView) dialog.findViewById(R.id.txt5);
        }
        if (dialog != null && !dialog.isShowing()) {
            view1.setText(data.getTYPE());
            view2.setText(data.getBatch());
            view3.setText(data.getRemark());
            view5.setText(data.getStatus());
            dialog.show();
        }
    }

    @Override
    public void onItemLongClick(Attendance data, int position) {
        Log.d(TAG, "onItemLongClick " + position);
        editData = data;
        position = position;
        if (editDialog == null) {
            editDialog = new Dialog(this,
                    android.R.style.DeviceDefault_Light_ButtonBar); // making dialog full screen
            //inflating dialog layout
            editDialog.setContentView(R.layout.edit_item);
            eview1 = (EditText) editDialog.findViewById(R.id.txt1);
            eview2 = (EditText) editDialog.findViewById(R.id.txt2);
            eview3 = (EditText) editDialog.findViewById(R.id.txt3);
            eview4 = (EditText) editDialog.findViewById(R.id.txt4);
            eview5 = (EditText) editDialog.findViewById(R.id.txt5);
        }
        if (editDialog != null && !editDialog.isShowing()) {
            eview1.setText(data.getTYPE());
            eview2.setText(data.getBatch());
            eview3.setText(data.getRemark());
            eview5.setText(data.getStatus());
            editDialog.show();
        }
    }

    public void onUpdateClick(View view) {
        Log.d(TAG, "onUpdateClick");
        try {
            if (editData != null) {
                Log.d(TAG, "onUpdateClick" + editData.getId());
                editData.setTYPE(eview1.getText().toString());
                editData.setBatch(eview2.getText().toString());
                editData.setRemark(eview3.getText().toString());

                dbQueries.open();
                if (dbQueries.updateRemark(editData)) {
                    Log.d(TAG, "onUpdateClick success" + editData.getId());
                    editDialog.dismiss();
                    editData = null;
                    adapterList = new DataAdapter(commonarraylist, CreateActivity.this);
                    recyclerView.setAdapter(adapterList);
                    Log.d(TAG, "L : " + commonarraylist.size());
                    adapterList.notifyDataSetChanged();

                }
                dbQueries.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDeleteClick(View view) {
        Log.d(TAG, "onDeleteClick " + position);
        try {
            if (editData != null) {
                Log.d(TAG, position + " onDeleteClick" + editData.getId());
                editData.setTYPE(eview1.getText().toString());
                editData.setBatch(eview2.getText().toString());
                editData.setRemark(eview3.getText().toString());

                dbQueries.open();
                if (dbQueries.deleteData(editData)) {
                    Log.d(TAG, "onDeleteClick success" + editData.getId());
                    editDialog.dismiss();
                    editData = null;
                    commonarraylist.remove(position);
                    adapterList = new DataAdapter(commonarraylist, CreateActivity.this);
                    recyclerView.setAdapter(adapterList);
                    Log.d(TAG, "L : " + commonarraylist.size());
                    adapterList.notifyDataSetChanged();
                }
                dbQueries.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
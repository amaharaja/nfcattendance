package com.olam.phyinv.db;

class DBConstants {

    static final String DATA_TABLE = "dataapp_table";
    static final String USER_TABLE = "user_table";
    static final String ID = "id";
    static final String USER_NAME = "user";
    static final String PASSWORD = "pwd";
    static final String TYPE = "type";
    static final String DEPART = "department";

    static final String PAGE = "pageType";
    static final String Remark = "remark";
    static final String DeviceId = "DeviceId";
    static final String Batch = "Batch";
    static final String Status = "Status";
    static final String DateIntiated = "DateI";
    static final String DateClosed = "dateC";

    static final String CREATE_DATA_TABLE = "CREATE TABLE IF NOT EXISTS " + DATA_TABLE + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PAGE + " TEXT,"
            + DeviceId + " TEXT,"
            + DEPART + " TEXT,"
            + TYPE + " TEXT,"
            + Batch + " TEXT,"
            + Remark + " TEXT,"
            + Status + " TEXT,"
            + DateIntiated + " TEXT,"
            + DateClosed + " TEXT,"
            + USER_NAME + " TEXT)";

    static final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS " + USER_TABLE + " ("
            + ID + " INTEGER PRIMARY KEY ,"
            + PASSWORD + " TEXT,"
            + DEPART + " TEXT,"
            + TYPE + " TEXT,"
            + Batch + " TEXT,"
            + Remark + " TEXT,"
            + Status + " TEXT,"
            + DateIntiated + " TEXT,"
            + USER_NAME + " TEXT)";
}

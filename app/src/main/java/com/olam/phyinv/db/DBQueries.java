package com.olam.phyinv.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.olam.phyinv.model.Attendance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.olam.phyinv.db.DBConstants.Batch;
import static com.olam.phyinv.db.DBConstants.DATA_TABLE;
import static com.olam.phyinv.db.DBConstants.ID;
import static com.olam.phyinv.db.DBConstants.PASSWORD;
import static com.olam.phyinv.db.DBConstants.USER_NAME;
import static com.olam.phyinv.db.DBConstants.USER_TABLE;

public class DBQueries {

    private Context context;
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public DBQueries(Context context) {
        this.context = context;
    }

    public DBQueries open() throws SQLException {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    // Attendance
    public long insertData(Attendance data) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.DeviceId, "" + data.getDeviceId());
        values.put(DBConstants.TYPE, "" + data.getTYPE());
        values.put(DBConstants.Batch, "" + data.getBatch());
        values.put(DBConstants.DEPART, "" + data.getDEPART());
        values.put(DBConstants.PAGE, "" + data.getPAGE());
        values.put(DBConstants.Remark, "" + data.getRemark());
        values.put(DBConstants.Status, "" + data.getStatus());
        values.put(DBConstants.DateIntiated, "" + data.getDateIntiated());
        values.put(DBConstants.DateClosed, "" + data.getDateClosed());
        values.put(DBConstants.USER_NAME, "" + data.getUsername());
        return database.insert(DATA_TABLE, null, values);
    }

    // Attendance Remark
    public boolean updateRemark(Attendance data) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.Remark, "" + data.getRemark());
        return database.update(DATA_TABLE, values, DBConstants.ID + "=?", new String[]{data.getId()}) > -1;
    }

    // Attendance Status
    public boolean updateStatus(List<Attendance> inventories) {
        boolean res = false;
        for (Attendance data : inventories) {
            ContentValues values = new ContentValues();
            values.put(DBConstants.Status, "H");
            res = database.update(DATA_TABLE, values, DBConstants.ID + "=?", new String[]{data.getId()}) > -1;
        }
        return res;
    }

    // Attendance
    public boolean deleteData(Attendance data) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.ID, data.getId());
        return database.delete(DATA_TABLE, DBConstants.ID + "=?", new String[]{data.getId()}) > -1;
    }

    public boolean deleteTable() {
        return database.delete(DATA_TABLE, null, null) > -1;
    }

    public List<Attendance> readData(String user, String depart) {
        List<Attendance> list = new ArrayList<>();
        try {
            database = dbHelper.getReadableDatabase();
            String selection = null;
            if (depart.equalsIgnoreCase("CSE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("EEE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("ECE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("Student")) {
                selection = DBConstants.USER_NAME + "='" + user + "'";
            }

            Cursor cursor = database.query
                    (
                            DATA_TABLE,
                            null,
                            selection,
                            null, null, null, null, null
                    );
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Attendance data = new Attendance();

                        data.setId(cursor.getString(cursor.getColumnIndex(DBConstants.ID)));
                        data.setTYPE(cursor.getString(cursor.getColumnIndex(DBConstants.TYPE)));
                        data.setBatch(cursor.getString(cursor.getColumnIndex(DBConstants.Batch)));
                        data.setRemark(cursor.getString(cursor.getColumnIndex(DBConstants.Remark)));
                        data.setStatus(cursor.getString(cursor.getColumnIndex(DBConstants.Status)));
                        data.setUsername(cursor.getString(cursor.getColumnIndex(DBConstants.USER_NAME)));
                        data.setDateIntiated(cursor.getString(cursor.getColumnIndex(DBConstants.DateIntiated)));
                        data.setDateClosed(cursor.getString(cursor.getColumnIndex(DBConstants.DateClosed)));

                        data.setDEPART(cursor.getString(cursor.getColumnIndex(DBConstants.DEPART)));
                        data.setPAGE(cursor.getString(cursor.getColumnIndex(DBConstants.PAGE)));

                        list.add(data);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        Log.v("list", "Size " + list.size());
        return list;
    }

    public List<Attendance> readROWLANEData(String user, String depart) {
        List<Attendance> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();

            String selection = null;
            if (depart.equalsIgnoreCase("CSE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("EEE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("ECE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("Student")) {
                selection = DBConstants.USER_NAME + "='" + user + "'";
            }
            cursor = database.query
                    (
                            DATA_TABLE,
                            null,
                            selection,
                            null, null, null, null, null
                    );

            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Attendance data = new Attendance();

                        data.setId(cursor.getString(cursor.getColumnIndex(DBConstants.ID)));
                        data.setDeviceId(cursor.getString(cursor.getColumnIndex(DBConstants.DeviceId)));
                        data.setTYPE(cursor.getString(cursor.getColumnIndex(DBConstants.TYPE)));
                        data.setBatch(cursor.getString(cursor.getColumnIndex(DBConstants.Batch)));
                        data.setRemark(cursor.getString(cursor.getColumnIndex(DBConstants.Remark)));
                        data.setStatus(cursor.getString(cursor.getColumnIndex(DBConstants.Status)));
                        data.setUsername(cursor.getString(cursor.getColumnIndex(DBConstants.USER_NAME)));
                        data.setDateIntiated(cursor.getString(cursor.getColumnIndex(DBConstants.DateIntiated)));
                        data.setDateClosed(cursor.getString(cursor.getColumnIndex(DBConstants.DateClosed)));

                        data.setDEPART(cursor.getString(cursor.getColumnIndex(DBConstants.DEPART)));
                        data.setPAGE(cursor.getString(cursor.getColumnIndex(DBConstants.PAGE)));

                        list.add(data);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        Log.v("list", "Size " + list.size());
        return list;
    }

    public List<String> readLANEData(String user, String depart) {
        List<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();

            String selection = null;
            if (depart.equalsIgnoreCase("CSE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("EEE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("ECE")) {
                selection = DBConstants.DEPART + "='" + depart + "'";

            } else if (depart.equalsIgnoreCase("Student")) {
                selection = DBConstants.USER_NAME + "='" + user + "'";
            }

            cursor = database.query
                    (
                            DATA_TABLE,
                            new String[]{DBConstants.DEPART},
                            selection,
                            null, DBConstants.DEPART, null, null, null
                    );
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        list.add(cursor.getString(cursor.getColumnIndex(DBConstants.DEPART)));
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        Log.v("list", "Size " + list.size());
        return list;
    }

    public boolean checkuser(String id, String batch, String depart, String name) {
        Cursor c = database.rawQuery(
                "SELECT * FROM " + USER_TABLE + " WHERE "
                        + ID + "='" + id + "'AND " + Batch + "='" + batch + "'", null);
        if (c.getCount() > 0) return true;
        return false;
    }

    public boolean userAdd(String id, String batch, String depart, String name, String num) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.ID, "" + id);
        values.put(DBConstants.Batch, "" + batch);
        values.put(DBConstants.DEPART, "" + depart);
        // Convert input Date into a String
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String date = inputFormat.format(new Date());

        values.put(DBConstants.DateIntiated, date);
        values.put(DBConstants.PASSWORD, "" + name + "pwd");
        values.put(DBConstants.USER_NAME, "" + name);
        values.put(DBConstants.TYPE, "" + num);
        return database.insert(DBConstants.USER_TABLE, null, values) > -1;
    }

    public boolean validateUser(String username, String password) {
        Cursor c = database.rawQuery(
                "SELECT * FROM " + USER_TABLE + " WHERE "
                        + USER_NAME + "='" + username + "'AND " + PASSWORD + "='" + password + "'", null);
        if (c.getCount() > 0)
            return true;
        return false;
    }

    public List<Attendance> readUser() {
        List<Attendance> list = new ArrayList<>();
        try {
            database = dbHelper.getReadableDatabase();
            Cursor cursor = database.query
                    (
                            DBConstants.USER_TABLE,
                            new String[]{DBConstants.ID, DBConstants.TYPE},
                            null, null, null, null, null, null
                    );

            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Attendance attendance = new Attendance();
                        attendance.setId(cursor.getString(cursor.getColumnIndex(DBConstants.ID)));
                        attendance.setTYPE(cursor.getString(cursor.getColumnIndex(DBConstants.TYPE)));
                        attendance.setUsername(cursor.getString(cursor.getColumnIndex(DBConstants.USER_NAME)));
                        list.add(attendance);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        Log.v("list", "Size " + list.size());
        return list;
    }

    public boolean isPersent(String id, String today) {
        Cursor c = database.rawQuery(
                "SELECT * FROM " + DATA_TABLE + " WHERE "
                        + DBConstants.DeviceId + "='" + id + "'AND " + DBConstants.PAGE + "='" + today + "'", null);
        if (c.getCount() > 0)
            return true;
        return false;
    }
}
package com.olam.phyinv.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.olam.phyinv.db.DBHelper;
import com.olam.phyinv.db.DBQueries;
import com.olam.phyinv.model.Attendance;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExcelToSQLite {

    private static Handler handler = new Handler(Looper.getMainLooper());

    private DBHelper dbHelper;
    private DBQueries dbQueries;
    private Context mContext;
    private SQLiteDatabase database;
    private ImportListener listener;
    private boolean dropTable = false;

    public ExcelToSQLite(Context context, boolean dropTable) {
        mContext = context;
        dbHelper = new DBHelper(mContext);
        dbQueries = new DBQueries(mContext);
        this.dropTable = dropTable;
        try {
            database = SQLiteDatabase
                    .openOrCreateDatabase(mContext.getDatabasePath(DBHelper.DB_NAME).getAbsolutePath(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void importFromAsset(final String assetFileName, final ImportListener listener) {
        if (listener != null) {
            listener.onStart();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    onReadData(mContext.getAssets().open(assetFileName));
                    if (listener != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onCompleted(DBHelper.DB_NAME);
                            }
                        });
                    }
                } catch (final Exception e) {
                    if (database != null && database.isOpen()) {
                        database.close();
                    }
                    if (listener != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(e);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    public void importFromFile(String filePath, ImportListener listener) {
        this.listener = listener;
        importFromFile(new File(filePath));
    }

    private void importFromFile(final File file) {
        if (listener != null) {
            listener.onStart();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    onReadData(new FileInputStream(file));
                    if (listener != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onCompleted(DBHelper.DB_NAME);
                            }
                        });
                    }
                } catch (final Exception e) {
                    if (database != null && database.isOpen()) {
                        database.close();
                    }
                    if (listener != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(e);
                            }
                        });
                    }
                }
            }
        }).start();

    }


    private void onReadData(InputStream stream) throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook(stream);
        XSSFSheet sheet = workbook.getSheetAt(0);
        final int rowsCount = sheet.getPhysicalNumberOfRows();
        FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
        dbQueries.open();
        for (int r = 0; r < rowsCount; r++) {
            Row row = sheet.getRow(r);
            int cellsCount = row.getPhysicalNumberOfCells();
            Attendance episode = new Attendance();

            episode.setId("ID" + new Date().getTime());
            for (int c = 0; c < cellsCount; c++) {
                String value = getCellAsString(row, c, formulaEvaluator);
                Log.d("DATA", "r:" + r + "; c:" + c + "; v:" + value);
                switch (c) {
                    case 0:
                        episode.setTYPE(value);
                        break;
                    case 1:
                        episode.setBatch(value);
                        break;
                    case 2:
                        episode.setRemark(value);
                        break;
                    case 3:
                        episode.setStatus(value);
                        break;
                    case 4:
                        episode.setStatus(value);
                        break;
                    case 5:
                        episode.setDateClosed(value);
                        break;
                    default:
                        break;
                }
            }

            if (listener != null) {
                final int finalR = r;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onProgress(finalR + " of " + rowsCount);
                    }
                });
            }
        }
        dbQueries.close();
    }

    private String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = "" + cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("dd/MM/yy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = "" + numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = "" + cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
      /* proper error handling should be here */
        }
        return value;
    }

    public interface ImportListener {

        void onStart();

        void onCompleted(String dbName);

        void onProgress(String per);

        void onError(Exception e);
    }

}
package com.olam.phyinv.util;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by Mushtaq on 14-04-2017.
 */

public class Utils {
    public static final String APPPREF = "AppPref";
    public static final String PREF_USER = "Pref_name";
    public static final String PREF_USER_TYPE = "Pref_name_type";
    public static final String PREF_FILE = "Pref_filename";
    public static final String PREF_FG = "Pref_fg";
    public static final String MyCurrentDate = "MyCurrentDate";
    public static final String  PREF_TOMAIL = "pref_tomail";

    public static void showSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }
}
